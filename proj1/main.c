#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>

void cd(char* argv[]);
void execute();
void execute_bg();

int main (int argc, char ** argv){
	char* prompt;
	prompt= "308sh> ";
	
	if (argc != 1 && argc != 3){
		printf("Error! Wrong argument count");
	}
	else if (argc == 3) {
		if (strcmp(argv[1], "-p") != 0)
			printf("Error! Add -p to include prompt");
	}
	else {
		prompt = argv[2];
	}

	//LOOP
	int runnning = 1;
	while (running) {
	
		//reading command

		if (!strcmp(argv[0] ,"exit"){
			break;
		} else if (!strcmp(argv[0] ,"pid"){
			printf("%ld", (long)getpid());
		} else if (!strcmp(argv[0] ,"ppid"){
			printf("%ld", (long)getppid());
		} else if (!strcmp(argv[0] ,"cd"){
			cd(argv);
		} else if (!strcmp(argv[0] ,"pwd"){
			printf("%s", getcwd());
		} else if (){
			
		} else {
			execute(argv);
		}
		/*
		int curStatus;
		pid_t process_child = waitpid(-1, &curstatus, WNOHANG);
		if (process_child > 0) {
			prinf("Exited process #%i \n", process_child);
			//TODO make a function to print status
		}
		*/
	}
	return 0;
}

//This function ensures that cd has proper paramters
//It uses chdir to change the directory
void cd(char* argv[]){
	if (argv[1] == NULL)
		printf("Please add a directory after cd\n");
	else
		if(chdir(argv[1]) == -1)
			printf("Cannot change to that directory\n", strerror(errno));
}

//main() is blocked while the process finishes
void execute(){
	pid_t pid = fork();
	if (pid == 0){
		printf("pid: %i\n", getpid());
		execvp(*argv, argv);
		printf("Error with command\n");
		exit(0);
	} else {
		int status;
		waidpid(pid, &status, 0);
		//TODO make a function to print status
	}
}
//main() is not blocked as the child process finishes
void execute_bg(){


